//
//  CreateContentEndpoint.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation

enum CreateContent {

    case Create(folder: String, isVideo: Bool)

    var path: String {
        switch self {
        case .Create(let folder, let isVideo):
            let fileType = isVideo ? 100 : 200
            return "http://52.1.242.26:8888/uploads?folder=\(folder)&fileType=\(fileType)"
        }
    }
}
