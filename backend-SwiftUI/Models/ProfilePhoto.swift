//
//  ProfilePhoto.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation


public struct ProfilePhotoRequest: Encodable {
    public var profilePhotoUrl: String
    
    enum CodingKeys: String, CodingKey {
        case profilePhotoUrl = "profilePhotoUrl"
    }
    
    public init(profilePhotoUrl: String) {
        self.profilePhotoUrl = profilePhotoUrl
    }
}
