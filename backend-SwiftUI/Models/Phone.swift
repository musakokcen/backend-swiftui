//
//  Phone.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation



public struct Phone: Codable {
    public let phoneNumber: String
    public let countryCode: String
    public let e164: String

    enum CodingKeys: String, CodingKey {
        case phoneNumber = "phoneNumber"
        case countryCode = "countryCode"
        case e164 = "e164"
    }

    public init(phoneNumber: String, countryCode: String, e164: String) {
        self.phoneNumber = phoneNumber
        self.countryCode = countryCode
        self.e164 = e164
    }
}
