//
//  UserEndpoint.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation

enum UserEndpoint {
    case Signin
    case Signup
    case Verify
    case ForgotPasswordCode
    case NewPassword
    case Me
    case PhotoUpdate
    case ProfileUpdate
    case UserExists
    case Academies

    var path: String {
        switch self {
        case .Signin:
            return "/auth/signin"
        case .Signup:
            return "/users/signup"
        case .Verify:
            return "/users/verify"
        case .ForgotPasswordCode:
            return "/users/forgotPassword"
        case .NewPassword:
            return "/users/newPassword"
        case .Me:
            return "/users/me"
        case .PhotoUpdate:
            return "/users/me/photo"
        case .ProfileUpdate:
            return "/users/me"
        case .UserExists:
            return "/users/existing-control"
        case .Academies:
            return "/users/academies"
        }
    }
    
    func url() -> URL? {
        guard let url = URL(string: Constants.ProductionServer.clientServiceBaseURL) else { return nil }
        return url.appendingPathComponent(path)
    }
    
    func urlRequest() -> URLRequest? {
        guard let url = URL(string: Constants.ProductionServer.clientServiceBaseURL) else { return nil }
        return  URLRequest(url: url.appendingPathComponent(path))
    }
}

