//
//  DeviceInfo.swift
//  backend-SwiftUI
//
//  Created by musa on 2.07.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import SwiftUI
import DeviceKit

class DeviceInfo {
    func deviceInfo() -> Device {
        let deviceId: String = UIDevice.current.identifierForVendor!.uuidString
        let os = "ios"
        let osVersion = UIDevice.current.systemVersion
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let deviceModel = DeviceKit.Device.current.description
        
        let device = Device(deviceId: deviceId, os: os, appVersion: appVersion, osVersion: osVersion, model: deviceModel)
        
        return device
    }
    deinit {
        print("device info deinited")
    }
}
