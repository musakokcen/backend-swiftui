//
//  SignUpRequest.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation

public struct SignupRequest: Encodable {
    public var email: String?
    public var firstname: String
    public var lastname: String
    public var password: String
    public var isCoach: Bool
    public var phone: Phone?
    public var device: Device
    public var facebook: Facebook?

    enum CodingKeys: String, CodingKey {
        case email = "email"
        case firstname = "firstname"
        case lastname = "lastname"
        case password = "password"
        case isCoach = "isCoach"
        case phone = "phone"
        case device = "device"
        case facebook = "facebook"
    }

    public init(email: String?, firstname: String, lastname: String, password: String, isCoach: Bool, phone: Phone?, device: Device, facebook: Facebook?) {
        self.email = email
        self.firstname = firstname
        self.lastname = lastname
        self.password = password
        self.isCoach = isCoach
        self.phone = phone
        self.device = device
        self.facebook = facebook
    }
}

public struct Facebook: Codable {
    public var id: String
    public var token: String

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case token = "token"
    }

    public init(id: String, token: String) {
        self.id = id
        self.token = token
    }
}
