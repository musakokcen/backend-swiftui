//
//  ContentView.swift
//  backend-SwiftUI
//
//  Created by musa on 2.07.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import SwiftUI



struct SignInView: View {
    
    @ObservedObject var viewModel = SignInViewModel()
    
    @State private var phoneNumber: String = ""
    @State private var email: String = ""
    @State private var password: String = ""

    @State private var showingAlert = false
    @State private var isEmail = true

    
    init() {
        UISegmentedControl.appearance().selectedSegmentTintColor = .gray
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.gray], for: .normal)

    }
    
    
    var body: some View {
        
        VStack {
            Spacer()
            VStack {
                Picker(selection: $isEmail, label: Text("")) {
                    Text("E-Mail").tag(true)
                    Text("Phone").tag(false)
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding(.bottom, 8)
                if self.isEmail
                {
                    TextField("E-Mail", text: $email)
                        .foregroundColor(.black)
                        .textContentType(.emailAddress)
                        .textFieldStyle(RoundedBorderTextFieldStyle.init())
                        .padding(.bottom, 8)
                }
                else
                {
                    TextField("Phone", text: $phoneNumber)
                        .foregroundColor(.black)
                        .textContentType(.telephoneNumber)
                        .textFieldStyle(RoundedBorderTextFieldStyle.init())
                        .foregroundColor(.black)
                        .padding(.bottom, 8)
                }


                    
                SecureField("Password", text: $password)
                    .foregroundColor(.black)
                    .textContentType(.password)
                    .textFieldStyle(RoundedBorderTextFieldStyle.init())
            }
            .padding()
            .padding(.bottom, 20)
            
            VStack {
                Button(action: {
                    
                    let userInfo = self.isEmail ? self.email : self.phoneNumber
                    
                    if
                        self.password != "" && userInfo != ""
                    {
                        self.viewModel.signIn(
                            user: userInfo,
                            password: self.password)
                    }
                    else
                    {
                        self.showingAlert.toggle()
                        
                    }
                }) {
                    Text("Sign In")
                }
                .padding(.bottom, 8)
                
                Button(action: {}) {
                    Text("Sign Up")
                }
                .padding(.bottom, 8)
                .alert(isPresented: $showingAlert){
                    Alert(title: Text("Error"), message: Text("Please enter pasword and either email or phone number"), dismissButton: .default(Text("OK")))
                }

                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                    Text("Forgot Password")
                }
            }
            .padding(.bottom, 30)

            VStack {
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                    Text("Sign Up With Face ID")
                }
                .padding(.bottom, 8)

                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                    Text("Sign Up With Facebook")
                }
            }
            Spacer()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}
