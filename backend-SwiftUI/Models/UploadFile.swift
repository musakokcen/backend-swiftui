//
//  Upload.swift
//  BackendTest-Viper
//
//  Created by musa on 7.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation


public struct UploadFile {
    public let assetURL: URL
    public let thumbnailURL: URL
    
    public init(assetURL: URL, thumbnailURL: URL) {
        self.assetURL = assetURL
        self.thumbnailURL = thumbnailURL
    }
}
