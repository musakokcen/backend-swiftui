//
//  AcademyEnpoint.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation

enum AcademyEndpoint {
    case Academies
    case GetAcademy(academyId: String)

    
    var path: String {
        switch self {
        case .Academies:
            return "/academies"
        case .GetAcademy(let academyId):
            return "/academies/\(academyId)"
        }
    }
    

    func url() -> URL? {
        guard let url = URL(string: Constants.ProductionServer.clientServiceBaseURL) else { return nil }
        return url.appendingPathComponent(path)
    }
}
