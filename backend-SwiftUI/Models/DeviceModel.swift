//
//  DeviceModel.swift
//  BackendTest-Viper
//
//  Created by musa on 26.03.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation

public struct Device: Codable {
    public var deviceId: String
    public var os: String
    public var appVersion: String
    public var osVersion: String
    public var model: String
}
