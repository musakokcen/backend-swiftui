//
//  ForgotPasswordCodeRequest.swift
//  BackendTest-Viper
//
//  Created by musa on 10.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation


public struct ForgotPasswordCodeRequest: Encodable {
    public var username: String

    enum CodingKeys: String, CodingKey {
        case username = "username"
    }

    public init(username: String) {
        self.username = username
    }
}
