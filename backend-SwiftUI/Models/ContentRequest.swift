//
//  commonModel.swift
//  BackendTest-Viper
//
//  Created by musa on 28.03.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import UIKit



public struct ContentRequest: Encodable {
    public var author: String
    public var name: String
    public var description: String
    public var orientation: Orientation
    public var contentType: Int
    public var slowMotionRate: Double
    public var assets: [Asset]
    public var academy: ContentAcademy?

    enum CodingKeys: String, CodingKey {
        case author = "author"
        case name = "name"
        case description = "description"
        case orientation = "orientation"
        case contentType = "contentType"
        case slowMotionRate = "slowMotionRate"
        case assets = "assets"
        case academy = "academy"
    }

    public init(author: String, name: String, description: String, orientation: Orientation, contentType: Int, slowMotionRate: Double, assets: [Asset], academy: ContentAcademy?) {
        self.author = author
        self.name = name
        self.description = description
        self.orientation = orientation
        self.contentType = contentType
        self.slowMotionRate = slowMotionRate
        self.assets = assets
        self.academy = academy
    }
}

public struct ContentAcademy: Encodable {
    public var academyId: String
    public var lessonId: String

    enum CodingKeys: String, CodingKey {
        case academyId = "academyId"
        case lessonId = "lessonId"
    }

    public init(academyId: String, lessonId: String) {
        self.academyId = academyId
        self.lessonId = lessonId
    }
}



