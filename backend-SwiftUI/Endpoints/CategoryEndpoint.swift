//
//  CategoryEndpoint.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation

enum CategoryEndpoint  {
    case Categories
    
    var path: String {
        switch self {
        case .Categories:
            return "/categories"
        }
    }
    
    
    func url() -> URL? {
        guard let url = URL(string: Constants.ProductionServer.clientServiceBaseURL) else { return nil }

        return  url.appendingPathComponent(path)
    }
}
