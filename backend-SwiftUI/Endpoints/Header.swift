
//
//  HEader.swift
//  backend-SwiftUI
//
//  Created by musa on 2.07.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation
import Alamofire

enum Headers {

    //case Create(String, String)
    case Content
    case Token(String)
    
    var header: HTTPHeaders {
        switch self {
        case .Content:
            return [
                "Content-Type":"application/json",
            ]
        case .Token(let user):
            return [
                "x-lvs-token": user
            ]
        }
    }

    
}
