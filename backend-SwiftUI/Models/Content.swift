//
//  Content.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation

public struct ContentResponse: Codable {
    public var contents: [ContentCategory]
    public var academies: [Academy]
    public var sliders: [Slider]

    enum CodingKeys: String, CodingKey {
        case contents = "contents"
        case academies = "academies"
        case sliders = "sliders"
    }

    public init(contents: [ContentCategory], academies: [Academy], sliders: [Slider]) {
        self.contents = contents
        self.academies = academies
        self.sliders = sliders
    }
}

// Content Category
public struct ContentCategory: Codable {
    public let id: String
    public let name: String
    public let contents: [Content]

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name = "name"
        case contents = "contents"
    }

    public init(id: String, name: String, contents: [Content]) {
        self.id = id
        self.name = name
        self.contents = contents
    }
}


public struct Content: Codable {
    public var id: String
    public var author: Author
    public var name: String
    public var description: String
    public var orientation: Orientation
    public var contentType: Int
    public var slowMotionRate: Double
    public var assets: [Asset]
    public var publishedAt: Date
    public var updatedAt: Date
    public var createdAt: Date

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case author = "author"
        case name = "name"
        case description = "description"
        case orientation = "orientation"
        case contentType = "contentType"
        case slowMotionRate = "slowMotionRate"
        case assets = "assets"
        case publishedAt = "publishedAt"
        case updatedAt = "updatedAt"
        case createdAt = "createdAt"
    }

    public init(id: String, author: Author, name: String, description: String, orientation: Orientation, contentType: Int, slowMotionRate: Double, assets: [Asset], publishedAt: Date, updatedAt: Date, createdAt: Date) {
        self.id = id
        self.author = author
        self.name = name
        self.description = description
        self.orientation = orientation
        self.contentType = contentType
        self.slowMotionRate = slowMotionRate
        self.assets = assets
        self.publishedAt = publishedAt
        self.updatedAt = updatedAt
        self.createdAt = createdAt
    }
}


public struct Author: Codable {
    public var id: String
    public var email: String?
    public var firstname: String
    public var lastname: String
    public var phone: Phone?
    public var isVerify: Bool
    public var isCoach: Bool
    public var updatedAt: Date
    public var createdAt: Date
    public var lastLoginAt: Date

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case email = "email"
        case firstname = "firstname"
        case lastname = "lastname"
        case phone = "phone"
        case isVerify = "isVerify"
        case isCoach = "isCoach"
        case updatedAt = "updatedAt"
        case createdAt = "createdAt"
        case lastLoginAt = "lastLoginAt"
    }

    public init(id: String, email: String?, firstname: String, lastname: String, phone: Phone?, isVerify: Bool, isCoach: Bool, updatedAt: Date, createdAt: Date, lastLoginAt: Date) {
        self.id = id
        self.email = email
        self.firstname = firstname
        self.lastname = lastname
        self.phone = phone
        self.isVerify = isVerify
        self.isCoach = isCoach
        self.updatedAt = updatedAt
        self.createdAt = createdAt
        self.lastLoginAt = lastLoginAt
    }
}

public struct Orientation: Codable {
    public let isPortrait: Bool
    public let isDouble: Bool

    enum CodingKeys: String, CodingKey {
        case isPortrait = "isPortrait"
        case isDouble = "isDouble"
    }

    public init(isPortrait: Bool, isDouble: Bool) {
        self.isPortrait = isPortrait
        self.isDouble = isDouble
    }
}

public struct Asset: Codable {
    public var assetUrl: String
    public var thumbnailUrl: String
    public var cameraView: Int
    public var isFirst: Bool
    public var isMirror: Bool
    public var size: VideoSize
    public var drawingsJSON: String?
    public var transitionJSON: String?
    public var timelineJSON: String?

    enum CodingKeys: String, CodingKey {
        case assetUrl = "assetURL"
        case thumbnailUrl = "thumbnailURL"
        case cameraView = "cameraView"
        case isFirst = "isFirst"
        case isMirror = "isMirror"
        case size = "size"
        case drawingsJSON = "drawings"
        case transitionJSON = "transition"
        case timelineJSON = "timeline"
    }

    public init(assetUrl: String, thumbnailUrl: String, cameraView: Int, isFirst: Bool, isMirror: Bool, size: VideoSize, drawingsJSON: String?, transitionJSON: String?, timelineJSON: String?) {
        self.assetUrl = assetUrl
        self.thumbnailUrl = thumbnailUrl
        self.cameraView = cameraView
        self.isFirst = isFirst
        self.isMirror = isMirror
        self.size = size
        self.drawingsJSON = drawingsJSON
        self.transitionJSON = transitionJSON
        self.timelineJSON = timelineJSON
    }
}

public struct VideoSize: Codable {
    public let original: Size
    public let onDevice: Size
    
    enum CodingKeys: String, CodingKey {
        case original = "original"
        case onDevice = "onDevice"
    }
    
    public init(original: Size, onDevice: Size) {
        self.original = original
        self.onDevice = onDevice
    }
}

public struct Size: Codable {
    public let w: Int
    public let h: Int

    enum CodingKeys: String, CodingKey {
        case w = "w"
        case h = "h"
    }
    
    public init(w: Int, h: Int) {
        self.w = w
        self.h = h
    }
}

// Academy
public struct Academy: Codable {
    public var id: String
    public var name: String
    public var description: String?
    public var thumbnailUrl: String
    public var headerUrl: String
    public var orderNo: Int
    public var lessons: [Lesson]?
    public var practices: [Practice]?
    public var createdAt: Date
    public var updatedAt: Date

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name = "name"
        case description = "description"
        case thumbnailUrl = "thumbnailUrl"
        case headerUrl = "headerUrl"
        case orderNo = "orderNo"
        case lessons = "lessons"
        case practices = "practices"
        case createdAt = "createdAt"
        case updatedAt = "updatedAt"
    }

    public init(id: String, name: String, description: String?,  thumbnailUrl: String, headerUrl: String, orderNo: Int, lessons: [Lesson]?, practices: [Practice]?, createdAt: Date, updatedAt: Date) {
        self.id = id
        self.name = name
        self.description = description
        self.thumbnailUrl = thumbnailUrl
        self.headerUrl = headerUrl
        self.orderNo = orderNo
        self.lessons = lessons
        self.practices = practices
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}


public struct Lesson: Codable {
    public var id: String
    public var name: String
    public var description: String?
    public var orderNo: Int
    public var thumbnailUrl: String
    public var videos: [LessonVideo]
    public var practices: [Practice]
    public var createdAt: Date
    public var updatedAt: Date
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name = "name"
        case description = "description"
        case orderNo = "orderNo"
        case thumbnailUrl = "thumbnailUrl"
        case videos = "videos"
        case practices = "practices"
        case createdAt = "createdAt"
        case updatedAt = "updatedAt"
    }

    public init(id: String, name: String, description: String?, orderNo: Int, thumbnailUrl: String, isDelete: Bool, status: Int, videos: [LessonVideo], practices: [Practice], createdAt: Date, updatedAt: Date) {
        self.id = id
        self.name = name
        self.description = description
        self.orderNo = orderNo
        self.thumbnailUrl = thumbnailUrl
        self.videos = videos
        self.practices = practices
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

public struct LessonVideo: Codable {
    public var name: String
    public var description: String?
    public var url: String
    public var orderNo: Int
    public var createdAt: Date
    public var updatedAt: Date

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case description = "description"
        case url = "videoUrl"
        case orderNo = "orderNo"
        case createdAt = "createdAt"
        case updatedAt = "updatedAt"
    }

    public init(name: String, description: String?, url: String, orderNo: Int, createdAt: Date, updatedAt: Date) {
        self.name = name
        self.description = description
        self.url = url
        self.orderNo = orderNo
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

public struct Practice: Codable {
    public var content: Content?
    public var orderNo: Int
    public var createdAt: Date
    public var updatedAt: Date

    enum CodingKeys: String, CodingKey {
        case content = "content"
        case orderNo = "orderNo"
        case createdAt = "createdAt"
        case updatedAt = "updatedAt"
    }

    public init(content: Content?, orderNo: Int, createdAt: Date, updatedAt: Date) {
        self.content = content
        self.orderNo = orderNo
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}


// Slider

public struct Slider: Codable {
    public var id: String
    public var title: String
    public var headline: String
    public var orderNo: Int
    public var type: Int
    public var contentUrl: String
    public var updatedAt: Date
    public var createdAt: Date

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case title = "title"
        case headline = "headline"
        case orderNo = "orderNo"
        case type = "type"
        case contentUrl = "contentUrl"
        case updatedAt = "updatedAt"
        case createdAt = "createdAt"
    }

    public init(id: String, title: String, headline: String, orderNo: Int, type: Int, contentUrl: String, updatedAt: Date, createdAt: Date) {
        self.id = id
        self.title = title
        self.headline = headline
        self.orderNo = orderNo
        self.type = type
        self.contentUrl = contentUrl
        self.updatedAt = updatedAt
        self.createdAt = createdAt
    }
}
