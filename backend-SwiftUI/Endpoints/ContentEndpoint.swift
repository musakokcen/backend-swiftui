//
//  ContentEndpoint.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation


enum ContentEndpoint {
    case Contents
    case NewContent
    case NewDemoContent
    case Sliders
    case ContentView(contentId: String)
    case ContentDownload(contentId: String)
    case GetContent(contentId: String)

    
    var path: String {
        switch self {
        case .Contents, .NewContent:
            return "/contents"
        case .NewDemoContent:
            return "/contents/demo"
        case .Sliders:
            return "/sliders"
        case .GetContent(let contentId):
            return "/contents/\(contentId)"
        case .ContentView(let contentId):
            return "/contents/\(contentId)/view"
        case .ContentDownload(let contentId):
            return "/contents/\(contentId)/download"
        }
    }
    
    
    func url() -> URL? {
        guard let url = URL(string: Constants.ProductionServer.clientServiceBaseURL) else { return nil }

        return url.appendingPathComponent(path)
    }
}
