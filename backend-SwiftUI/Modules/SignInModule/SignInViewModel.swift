//
//  SignInViewModel.swift
//  backend-SwiftUI
//
//  Created by musa on 2.07.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation
import Combine
import Alamofire

struct UserMessage<T: Codable>: Codable {
  let message: T
  let status: User
}
class SignInViewModel: ObservableObject {
    @Published var user: [User]?

  private var task: AnyCancellable?
  
  
  func fetchUser() {
    guard let url = URL(string: "") else { return }
    
    task = URLSession.shared.dataTaskPublisher(for: url)
    .map { $0.data }
    .decode(type: UserMessage<[User]>.self, decoder: JSONDecoder())
    .map { $0.message }
    .replaceError(with: [User]())
    .eraseToAnyPublisher()
    .receive(on: RunLoop.main)
    .assign(to: \SignInViewModel.user, on: self)
    
  }
  
    
    func signIn(user: String, password: String){
        let headers = Headers.Content.header
        let device = DeviceInfo().deviceInfo()
        guard let url = UserEndpoint.Signin.url() else {print("url could not be created");return}
        let body = SigninRequest(username: user, password: password, device: device)
        
        do
            {
                let bodyJson = try body.toDictionary()
                
                AF.request(url.absoluteURL, method: .post, parameters: bodyJson, encoding: JSONEncoding.default, headers: headers)
                    .validate(statusCode: 200..<300)
                    .responseJSON{ response in
                        switch response.result {
                        case .success:
                            print("success")
                            guard let data = response.data else { return }
                            do {
                                print("decode now...")
                                let decoder = JSONDecoder()
                                let df = DateFormatter()
                                df.dateFormat =  "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                                decoder.dateDecodingStrategy = .formatted(df)

                                // MARK: USER SIGNED IN - Decode user, Save UserDefaults, Perform Segue with User Info
                            
                                let user = try decoder.decode(User.self, from: data)
                                UserDefaults.standard.setStruct(user, forKey: "user_info")
                               // self.presenter?.UserInfoFetched(userInfo: user, isVerified: user.isVerify)
                                
                                
                            } catch let error {
                                print("decoding err ",error)
                            }
                            
                        case .failure(let error):
                            print("HTTP Request failed", error)
                           // self.presenter?.UserInfoFetchFailed(with: error.localizedDescription)
                            
                        }
                }
            }
        catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    func signUp(){
        
    }
    
    func forgotPassword(){
        
    }
  
    func logInWitFacebook(){
        
    }
    func logInWithAppleId(){
        
    }
    func logInWithFaceID(){
        
    }
    
    
}
