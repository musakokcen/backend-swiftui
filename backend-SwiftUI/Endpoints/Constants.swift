//
//  Constants.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation


struct Constants {
    struct ProductionServer {
        static let clientServiceBaseURL = "https://client-service.dev.lvs.global"
        static let fileServiceBaseURL = "http://52.1.242.26:8888"
    }
}


