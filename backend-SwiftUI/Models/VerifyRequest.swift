//
//  VerifyRequest.swift
//  BackendTest-Viper
//
//  Created by musa on 10.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation

public struct VerifyRequest: Encodable {
    public var verifyCode: String

    enum CodingKeys: String, CodingKey {
        case verifyCode = "verifyCode"
    }

    public init(verifyCode: String) {
        self.verifyCode = verifyCode
    }
}
