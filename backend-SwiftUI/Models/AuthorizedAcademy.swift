//
//  AuthorizedAcademy.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation


public struct AuthorizedAcademy: Codable {
    public var id: String
    public var name: String
    public var lessons: [AuthorizedLesson]

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case lessons = "lessons"
    }

    public init(id: String, name: String, lessons: [AuthorizedLesson]) {
        self.id = id
        self.name = name
        self.lessons = lessons
    }
}

public struct AuthorizedLesson: Codable {
    public var id: String
    public var name: String

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }

    public init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
