//
//  NewContent.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import UIKit

public struct NewContentRequest {
    public var author: String
    public var name: String
    public var description: String
    public var orientation: Orientation
    public var contentType: Int
    public var slowMotionRate: Double
    public var assets: [NewAssetRequest]
    public var academy: NewContentAcademyRequest?

    public init(author: String, name: String, description: String, orientation: Orientation, contentType: Int, slowMotionRate: Double, assets: [NewAssetRequest], academy: NewContentAcademyRequest?) {
        self.author = author
        self.name = name
        self.description = description
        self.orientation = orientation
        self.contentType = contentType
        self.slowMotionRate = slowMotionRate
        self.assets = assets
        self.academy = academy
    }
}

public struct NewAssetRequest {
    public var assetUrl: URL
    public var thumbnail: UIImage
    public var cameraView: Int
    public var isFirst: Bool
    public var isMirror: Bool
    public var size: VideoSize
    public var drawingsJSON: String
    public var transitionJSON: String
    public var timelineJSON: String

    public init(assetUrl: URL, thumbnail: UIImage, cameraView: Int, isFirst: Bool, isMirror: Bool, size: VideoSize, drawingsJSON: String, transitionJSON: String, timelineJSON: String) {
        self.assetUrl = assetUrl
        self.thumbnail = thumbnail
        self.cameraView = cameraView
        self.isFirst = isFirst
        self.isMirror = isMirror
        self.size = size
        self.drawingsJSON = drawingsJSON
        self.transitionJSON = transitionJSON
        self.timelineJSON = timelineJSON
    }
}

public struct NewContentAcademyRequest {
    public var academyId: String
    public var lessonId: String

    public init(academyId: String, lessonId: String) {
        self.academyId = academyId
        self.lessonId = lessonId
    }
}
