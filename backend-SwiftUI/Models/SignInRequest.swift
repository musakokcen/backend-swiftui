//
//  SignInRequest.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation


public struct SigninRequest: Encodable {
    public var username: String
    public var password: String
    public var device: Device
    

    enum CodingKeys: String, CodingKey {
        case username = "username"
        case password = "password"
        case device = "device"
    }

    public init(username: String, password: String, device: Device) {
        self.username = username
        self.password = password
        self.device = device
    }
}
