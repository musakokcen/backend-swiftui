//
//  User.swift
//  BackendTest-Viper
//
//  Created by musa on 8.04.2020.
//  Copyright © 2020 musa. All rights reserved.
//

import Foundation


public struct User: Codable {
    public let id: String
    public let email: String?
    public let firstname: String
    public let lastname: String
    public let isCoach: Bool
    public let isVerify: Bool
    public let phone: Phone?
    public let auth: Auth
    public let gender: Int?
    public let birthday: Date?
    public let profilePhotoUrl: String?
    public let isContentPublisher: Bool?
    public let lastLogin : Date
    public let created : Date
    public let updatedAt : Date

 

    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case email = "email"
        case firstname = "firstname"
        case lastname = "lastname"
        case phone = "phone"
        case isCoach = "isCoach"
        case isVerify = "isVerify"
        case auth = "auth"
        case gender = "gender"
        case birthday = "birthday"
        case profilePhotoUrl = "profilePhotoUrl"
        case isContentPublisher = "isContentPublisher"
        case lastLogin = "lastLoginAt"
        case created = "createdAt"
        case updatedAt = "updatedAt"
    }
    
    public init(id: String, email: String?, firstname: String, lastname: String, phone: Phone?, isCoach: Bool, isVerify: Bool, auth: Auth, gender: Int?, birthday: Date?, profilePhotoUrl: String?, isContentPublisher: Bool?, lastLogin : Date, created : Date, updatedAt: Date) {
        self.id = id
        self.email = email
        self.firstname = firstname
        self.lastname = lastname
        self.phone = phone
        self.isCoach = isCoach
        self.isVerify = isVerify
        self.auth = auth
        self.gender = gender
        self.birthday = birthday
        self.profilePhotoUrl = profilePhotoUrl
        self.isContentPublisher = isContentPublisher
        self.lastLogin = lastLogin
        self.created = created
        self.updatedAt = updatedAt
    }
}

public struct Auth: Codable {
    public let token: String
    public let refreshToken: String
    public let expiresAt: Date

    enum CodingKeys: String, CodingKey {
        case token = "token"
        case refreshToken = "refreshToken"
        case expiresAt = "expiresAt"
    }
    
    public init(token: String, refreshToken: String, expiresAt: Date) {
        self.token = token
        self.refreshToken = refreshToken
        self.expiresAt = expiresAt
    }
}
